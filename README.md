# Automated Deployment of a Nautobot Container on GCP using Terraform

### Schéma directeur du projet :
![Project Infra](documents/schema_infra.png "Schéma Projet")

## **Objectifs :**

Ce dépôt permet le déploiement d'une infrastructure avec un conteneur Nautobot et un LoadBalancer sur GCP via Terraform.

Le conteneur Nautobot est accessible via "http://IP:80", où IP est l'adresse IP publique attribuée à l'équilibreur de charge. Cette adresse IP sera affichée à la fin du pipeline de déploiement.

Pour réduire les coûts, l'adresse IP attribuée à l'équilibreur de charge est une adresse IP éphémère, ce qui signifie qu'elle sera libérée et remplacée par une nouvelle adresse IP a chaque destruction et reconstruction du projet.


## **Points Importants**

 ### Variables :
 1. GCP exige une authentification via un compte personnel Google ou un compte de service. (cf : [ici](https://cloud.google.com/docs/authentication/gcloud?hl=fr) )
 2. Pour se connecter au projet, il est nécessaire d'utiliser l'identifiant unique (UID) du projet. (cf : [ici](https://cloud.google.com/resource-manager/docs/creating-managing-projects?hl=fr#gcloud_1) ) puis se connecter au projet (cf : [ici](https://cloud.google.com/sdk/gcloud/reference/config/set) )
 3. Nota : Il y a un quota de projet, 7 pour la version gratuite, CE N'EST PAS comme les Resources Groups sous Azure.
 ### Versions des packages utilisés dans le Dockerfile :
 * Version de gCloud CLI : **>=478.0.0**.
 * Version de Terraform : **1.8.3**.
 * Version du package gCloud CLI de Terraform : **5.29.1**.

 ### Utilisation d'une image docker pré-installer:
 Pour permettre une exécution plus rapide du pipeline lors des tests, au lieu d'installer gCloud CLI et Terraform à chaque nouveau pipeline, j'ai créé un Dockerfile afin de gérer l'installation des dépendances pendant le processus du pipeline. Vous pouvez retrouver le Dockerfile utilisé ici : '/dockerfile/Dockerfile'.

 ### old_file disponible :
 Les fichiers disponibles sous /old_file permettent de comprendre les différentes étapes de vie du projet. Ils sont ici uniquement à titre de base de connaissances.

 ### Impact environnemental :
Le projet étant un projet d'étudiant français, il était important de choisir une région de travail française. Google met à disposition une région France, localisée dans la région parisienne. D'après les informations publiques disponibles, la région europe-west9 est la 5ᵉ sur l'ensemble des 36 régions de Google disponibles.


![europe-west9 Carbon Data](documents/europe-west9_eco.png "Carbon Data europe-west9")

Schéma des régions suivant les préconisations du Lower Carbon ainsi que les [objectifs annuels de Google](https://cloud.google.com/blog/topics/inside-google-cloud/announcing-round-the-clock-clean-energy-for-cloud?hl=en) concernant la sobriété énergétique des centres de données.
![Grid Region Carbon](documents/grid-region-carbon.png "Grid Carbon List Region")


## **Installation**
### **Installation Manuelle sous LINUX**
* Sur GCP :
   1. Crée un projet : [Ici](https://cloud.google.com/appengine/docs/standard/python3/building-app/creating-gcp-project?hl=fr)
   2. Crée un compte de service : [Ici](https://cloud.google.com/iam/docs/service-accounts-create?hl=fr)
   3. Donner les droits IAM au compte de service sur le projet : [Ici](https://cloud.google.com/iam/docs/manage-access-service-accounts?hl=fr#iam-grant-single-role-sa-gcloud)
   4. Modifier les valeurs du fichier "variables.tf" selon vos informations.
   5. 

Sur le [WSL](https://learn.microsoft.com/fr-fr/windows/wsl/install) ou sur une machine virtuelle :
 * [Installer](https://cloud.google.com/sdk/docs/install?hl=fr) l'outil gCloud CLI.
 * [Installer](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli#install-terraform) l'outil Terraform.
 
 * Sur Linux :
   - Cloner le dépot terraform : `git clone https://gitlab.com/ZanKar/fil-rouge.git`
   - Naviguer dans le dossier du projet : `cd ~/fil-rouge/`.
   - Se connecter à son compte GCP : `gcloud init`.
   - Initialiser Terraform : `terraform init`.
   - Valider la configuration : `terraform validate`.
   - Projeter la configuration sur GCP : `terraform plan`.
   - Appliquer l'infrastructure dans GCP : `terraform apply`.
   - Supprimer l'infrastructure dans GCP: `terraform destroy`.
 

## **Sources**
* Activation automatisé des services API : [Ici](https://stackoverflow.com/a/72094901) 
* Libération de l'adresse IP serverless : [Ici](https://stackoverflow.com/questions/78524117/failed-to-release-serverless-ipv4-because-in-use-by-some-address-reservation)
* Module LoadBalancer : [Ici](https://registry.terraform.io/modules/GoogleCloudPlatform/lb-http/google/latest/submodules/serverless_negs) 
* Image Docker Google CLI : [Ici](https://cloud.google.com/sdk/docs/downloads-docker?hl=fr)
* Region Carbon Data : [Ici](https://cloud.google.com/sustainability/region-carbon#data)
* Rapport Annuel des CO2 par région : [Ici](https://github.com/GoogleCloudPlatform/region-carbon-info/blob/main/data/yearly/2022.csv)



