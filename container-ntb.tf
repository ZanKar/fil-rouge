resource "google_cloud_run_v2_service" "default" {
  name     = var.containername
  location = var.location
  ingress  = "INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER"
    template {
        scaling {
            max_instance_count = 1
        }
        containers {
            image = "networktocode/nautobot-lab"
            ports {
                container_port = 8000
            }
            resources {
                limits = {
                    cpu    = "4"
                    memory = "4096Mi"
                }
            }
        }
        vpc_access{
            network_interfaces {
                network    = google_compute_network.vnnautobot.name
                subnetwork = google_compute_subnetwork.subnetnautobot.name
            }
            egress = "ALL_TRAFFIC"
        }
    }
}

# Permet à tous d'acceder au conteneur Nautobot , car site web public
resource "google_cloud_run_service_iam_binding" "default" {
  location = var.location
  service  = google_cloud_run_v2_service.default.name
  role     = "roles/run.invoker"
  members  = ["allUsers"]
  depends_on = [google_cloud_run_v2_service.default]
}



