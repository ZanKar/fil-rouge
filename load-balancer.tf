resource "google_compute_region_network_endpoint_group" "cloudrun_neg" {
  name                  = var.containername
  network_endpoint_type = "SERVERLESS"
  region                = var.location
  cloud_run {
    service = google_cloud_run_v2_service.default.name
  }
  #depends_on = [google_project_service.gcp_services]
}


module "lb-http" {
  source            = "GoogleCloudPlatform/lb-http/google//modules/serverless_negs"
  version           = "~> 9.0"
  project           = var.project
  name              = "load-balancer"
  ssl               = false
  https_redirect    = false
  create_address    = false
  http_forward      = true
  load_balancing_scheme = "EXTERNAL_MANAGED"

  backends = {
    default = {
      protocol                        = "HTTP"
      enable_cdn                      = false

      log_config = {
        enable = false
      }

      groups = [
        {
          group = google_compute_region_network_endpoint_group.cloudrun_neg.id
        }
      ]
      iap_config = {
        enable               = false
      }
    }
  }
  depends_on = [google_cloud_run_v2_service.default]
}

resource "google_compute_subnetwork" "subnetnautobot" {
  name          = "subnetnautobot"
  ip_cidr_range = "192.168.0.0/24"
  region        = var.location
  network       = google_compute_network.vnnautobot.id
  depends_on = [google_compute_network.vnnautobot]
}

resource "google_compute_network" "vnnautobot" {
  name                    = "vnnautobot"
  auto_create_subnetworks = true
}