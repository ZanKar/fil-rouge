terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = ">=5.29.1"
    }
  }
  backend "gcs" {
   bucket  = "nicolasgiltest-filrouge-tfstate"
   prefix  = "terraform/state"
 }
}

provider "google" {
    project = var.project
    region  = var.location

}