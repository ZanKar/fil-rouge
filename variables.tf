variable "location" {
  description = "Région gCloud pour créer les ressources"
  type        = string
  default     = "europe-west9"
}
variable "project" {
  description = "Région gCloud pour créer les ressources"
  type        = string
  default     = "trfm-fil-rouge"
}
variable "containername" {
  description = "Nom du conteneur"
  type        = string
  default     = "nautobotcontainer"
}